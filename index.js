const printTimeout = process.argv[2] | 1000;

setTimeout(() => {
    process.stdout.write(JSON.stringify({
        scoreA: 1010,
        scoreB: 420
    }));
}, printTimeout);