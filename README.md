# Dummy Node.js dockerized

## App

**Run** :
```shell script
node index.js [print_timeout]
```
> **print_timeout** : number = ms before printing

## Docker
**Build** :
```shell script
docker build . -t dummy-nodejs
```
